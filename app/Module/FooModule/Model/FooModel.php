<?php

namespace App\Module\FooModule\Model;

use App\Components\DatabaseOrder;
use App\Components\Foo\ActiveRow;
use App\Components\Foo\Explorer;
use App\Components\Helper\EntityConverter;
use App\Components\Interface\EntityInterface;
use App\Components\Interface\ModelInterface;
use App\Components\Interface\TableInterface;
use App\Components\Trait\ModelTrait;
use App\Module\FooModule\Components\FooTable;

class FooModel implements ModelInterface
{
	use ModelTrait;

	public function __construct(Explorer $explorer)
	{
		$this->setDatabase($explorer, FooTable::TABLE_NAME);
	}


	public function getItem(int $itemId): ?ActiveRow
	{
		return $this->getTable()->get($itemId);
	}


	public function getItemByField(TableInterface $field, mixed $value): ?ActiveRow
	{
		return $this->getTable()
			->where($field->getValue(), $value)
			->fetch();
	}


	public function getItems(?DatabaseOrder $order = NULL, ?int $limit = NULL): array
	{
		$sql = $this->getTable();

		if ($order) {
			$sql->order(sprintf('%s %s', FooTable::CREATED_AT->getValue(), $order->value));
		}

		return $sql->limit($limit)->fetchAll();
	}


	public function getItemsByField(
		TableInterface $field,
		mixed $value,
		?DatabaseOrder $order,
		?int $limit = NULL
	): array {
		// TODO ...
	}


	public function insert(EntityInterface $entity): ActiveRow
	{
		$result = EntityConverter::convert($entity);

		return $this->getTable()->insert($result->getData());
	}


	public function update(EntityInterface $entity): void
	{
		$result = EntityConverter::convert($entity);

		$this->getTable()->update($result->getEntityId(), $result->getData());
	}


	public function delete(EntityInterface $entity): void
	{
		$this->getTable()->delete($entity->getId());
	}


}