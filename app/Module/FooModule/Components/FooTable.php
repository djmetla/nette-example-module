<?php

namespace App\Module\FooModule\Components;

use App\Components\Interface\TableInterface;

enum FooTable: string implements TableInterface
{
	case TABLE_NAME = 'foo_table';
	case ID = 'id';
	case CREATED_AT = 'created_at';
	case CREATED_BY = 'created_by';
	case CONTENT = 'content';
	case ACTIVE = 'active';


	public function getValue(): string
	{
		return $this->value;
	}

}