<?php

namespace App\Module\FooModule\Components;

use App\Components\Foo\ActiveRow;
use App\Components\Helper\EntityBuilder;
use App\Components\Interface\BuilderInterface;
use App\Components\Interface\EntityInterface;
use App\Module\FooModule\Entity\FooEntity;

class FooEntityBuilder implements BuilderInterface
{

	public static function build(ActiveRow $row, array $payload = []): FooEntity
	{
		return (new FooEntity())
			->setId($row->offsetGet(FooTable::ID->getValue()))
			->setActive($row->offsetGet(FooTable::ACTIVE->getValue()))
			->setContent($row->offsetGet(FooTable::CONTENT->getValue()))
			->setCreatedBy($row->offsetGet(FooTable::CREATED_BY->getValue()))
			->setCreatedAt($row->offsetGet(FooTable::CREATED_AT->getValue()));
	}


	public static function buildRows(array $rows): array
	{
		return EntityBuilder::buildRows($rows,self::class);
	}


}