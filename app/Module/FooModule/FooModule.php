<?php

namespace App\Module\FooModule;

use App\Components\DatabaseOrder;
use App\Components\Foo\ArrayHash;
use App\Module\FooModule\Components\FooEntityBuilder;
use App\Module\FooModule\Components\FooTable;
use App\Module\FooModule\Entity\FooEntity;
use App\Module\FooModule\Exceptions\FooEntityNotFoundException;
use App\Module\FooModule\Model\FooModel;
use DateTimeImmutable;

class FooModule
{
	private FooModel $fooModel;

	public function __construct(FooModel $fooModel)
	{
		$this->fooModel = $fooModel;
	}


	public function getFoo(int $fooId): FooEntity
	{
		$entity = $this->fooModel->getItem($fooId);

		if (!$entity) {
			throw new FooEntityNotFoundException(sprintf('Foo entity with id #%d not found', $fooId));
		}

		return FooEntityBuilder::build($entity);
	}


	public function getMultipleFoo(): array
	{
		return FooEntityBuilder::buildRows($this->fooModel->getItems(DatabaseOrder::ASC, 50));
	}


	public function createFromForm(ArrayHash $formData): FooEntity
	{
		$fooEntity = new FooEntity();
		$fooEntity->setCreatedBy($this->getUser()->getId());
		$fooEntity->setCreatedAt(new DateTimeImmutable());
		$fooEntity->setContent($formData->offsetGet(FooTable::CONTENT->getValue()));
		$fooEntity->setActive(TRUE);

		return FooEntityBuilder::build($this->fooModel->insert($fooEntity));
	}

	// ETC, update, delete...
}