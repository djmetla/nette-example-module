<?php

namespace App\Module\FooModule\Entity;

use App\Components\Attributes\DBField;
use App\Components\Interface\EntityInterface;
use App\Module\FooModule\Components\FooTable;
use DateTimeInterface;

class FooEntity implements EntityInterface
{
	private int $id;

	#[DBField(fieldName: FooTable::CREATED_AT)]
	private DateTimeInterface $createdAt;

	#[DBField(fieldName: FooTable::CREATED_BY)]
	private int $createdBy;

	#[DBField(fieldName: FooTable::CONTENT)]
	private string $content;

	#[DBField(fieldName: FooTable::ACTIVE)]
	private bool $active;


	public function getId(): int
	{
		return $this->id;
	}


	public function setId(int $id): self
	{
		$this->id = $id;

		return $this;
	}


	public function getCreatedAt(): DateTimeInterface
	{
		return $this->createdAt;
	}


	public function setCreatedAt(DateTimeInterface $createdAt): self
	{
		$this->createdAt = $createdAt;

		return $this;
	}


	public function getCreatedBy(): int
	{
		return $this->createdBy;
	}


	public function setCreatedBy(int $createdBy): self
	{
		$this->createdBy = $createdBy;

		return $this;
	}


	public function getContent(): string
	{
		return $this->content;
	}


	public function setContent(string $content): self
	{
		$this->content = $content;

		return $this;
	}


	public function isActive(): bool
	{
		return $this->active;
	}


	public function setActive(bool $active): self
	{
		$this->active = $active;

		return $this;
	}


}