<?php

namespace App\Components\Trait;

use App\Components\Foo\Explorer;
use App\Components\Foo\Selection;
use App\Components\Interface\TableInterface;

trait ModelTrait
{
	private ?string $tableName = NULL;
	private Explorer $explorer;


	public function setDatabase(Explorer $explorer, ?TableInterface $tableName = NULL): void
	{
		$this->explorer = $explorer;
		$this->tableName = $tableName?->getValue();
	}


	public function getExplorer(): Explorer
	{
		return $this->explorer;
	}


	public function getTable(): Selection
	{
		return $this->explorer->table($this->tableName);
	}


}