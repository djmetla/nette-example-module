<?php

namespace App\Components\Foo;

class ActiveRow
{
	// fake class

	public function offsetGet(string $offset): mixed
	{
		return '1';
	}

	public function ref(string $offset): mixed
	{
		return '2';
	}
}