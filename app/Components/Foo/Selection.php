<?php

namespace App\Components\Foo;

class Selection
{
	public function get(int $id): ActiveRow
	{
		return new ActiveRow();
	}

	public function insert(iterable $data): ActiveRow
	{
		return new ActiveRow();
	}

	public function fetchAll(): array
	{
		return [];
	}

	public function update(int $itemId, iterable $data): void
	{
		return;
	}

	public function delete(int $itemId): void
	{
		return;
	}
}