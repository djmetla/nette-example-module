<?php

namespace App\Components\Interface;

use App\Components\DatabaseOrder;
use App\Components\Foo\ActiveRow;

interface ModelInterface
{

	public function getItem(int $itemId): ?ActiveRow;

	public function getItemByField(TableInterface $field, mixed $value): ?ActiveRow;

	public function getItems(?DatabaseOrder $order = NULL, ?int $limit = NULL): array;

	public function getItemsByField(TableInterface $field, mixed $value, ?DatabaseOrder $order, ?int $limit = NULL): array;

	public function insert(EntityInterface $entity): ActiveRow;

	public function update(EntityInterface $entity): void;

	public function delete(EntityInterface $entity): void;
}