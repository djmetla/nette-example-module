<?php

namespace App\Components\Interface;

use App\Components\Foo\ActiveRow;

interface BuilderInterface
{

	public static function build(ActiveRow $row, array $payload = []): EntityInterface;

	public static function buildRows(array $rows): array;

}