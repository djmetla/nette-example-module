<?php

namespace App\Components\Interface;

interface EntityInterface
{
	public function getId(): int;
}