<?php

namespace App\Components\Interface;

interface TableInterface
{
	public function getValue(): string;
}