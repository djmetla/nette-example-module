<?php

namespace App\Components\Attributes;

use App\Components\Interface\TableInterface;
use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class DBField
{

	private TableInterface $fieldName;

	public function __construct(TableInterface $fieldName)
	{
		$this->fieldName = $fieldName;
	}

	public function getFieldName(): TableInterface
	{
		return $this->fieldName;
	}

}