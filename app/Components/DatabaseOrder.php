<?php

namespace App\Components;

enum DatabaseOrder: string
{
	case DESC = 'DESC';
	case ASC = 'ASC';
}