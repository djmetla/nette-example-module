<?php

namespace App\Components\Helper\Container;

class EntityConverterResult
{
	private int $entityId;
	private array $data;

	public function __construct(
		int $entityId,
		array $data,
	)
	{
		$this->entityId = $entityId;
		$this->data = $data;
	}

	public function getEntityId(): int
	{
		return $this->entityId;
	}

	public function getData(): array
	{
		return $this->data;
	}


}