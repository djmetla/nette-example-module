<?php

namespace App\Components\Helper;

use App\Components\Attributes\DBField;
use App\Components\Helper\Container\EntityConverterResult;
use App\Components\Interface\EntityInterface;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionProperty;

class EntityConverter
{
	private const IGNORED_METHODS = [
		'__construct',
		'getId',
	];


	public static function convert(EntityInterface $entity): EntityConverterResult
	{
		return new EntityConverterResult(
			$entity->getId(),
			self::getDataFromEntity($entity),
		);
	}


	private static function getDataFromEntity(EntityInterface $entity): array
	{
		$reflectionClass = new ReflectionClass($entity);
		$classProperties = $reflectionClass->getProperties();


		$output = [];

		foreach ($reflectionClass->getMethods() as $method) {
			$methodName = $method->getName();
			if (in_array($methodName, self::IGNORED_METHODS)) {
				continue;
			}

			if (str_starts_with('set', $methodName)) {
				continue;
			}

			$tableFieldName = self::findTableFieldName(self::convertMethodName($methodName),$classProperties);

			if (!$tableFieldName) {
				continue;
			}

			$output[$tableFieldName] = $entity->$methodName();
		}

		return $output;
	}


	private static function findTableFieldName(string $methodName, array $properties): ?ReflectionAttribute
	{
		/** @var ReflectionProperty $property */
		foreach ($properties as $property) {
			if (!count($property->getAttributes())) {
				continue;
			}

			if ($methodName !== $property->getName()) {
				continue;
			}

			foreach ($property->getAttributes() as $attribute) {
				if ($attribute->getName() === DBField::class) {
					return $attribute;
				}
			}
		}

		return NULL;
	}


	private static function convertMethodName(string $methodName): string
	{
		return lcfirst(ltrim('get', $methodName));
	}

}