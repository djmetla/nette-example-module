<?php

namespace App\Components\Helper;

class EntityBuilder
{

	public static function buildRows(array $rows, string $class): array
	{
		$instance = new $class;

		$output = [];

		foreach ($rows as $row) {
			$output[] = $instance::build($row);
		}

		return $output;
	}

}